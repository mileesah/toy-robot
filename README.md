# TOY ROBOT SIMULATION

This is a command line Java application that simulates iteration with custom commands

## Description

The application is a simulation of a toy robot moving on a square tabletop,
 of dimensions 5 units x 5 units.
 
- There are no other obstructions on the table surface.
- The robot is free to roam around the surface of the table, but must be
  prevented from falling to destruction. Any movement that would result in the
  robot falling from the table must be prevented, however further valid
  movement commands must still be allowed. 

### List of Commands

	PLACE X,Y,(NORTH|SOUTH|EAST|WEST)
    LEFT
    RIGHT
    MOVE
    REPORT

- PLACE will put the toy robot on the table in position X,Y and facing NORTH,
  SOUTH, EAST or WEST.
- The origin (0,0) can be considered to be the SOUTH WEST most corner.
- The first valid command to the robot is a PLACE command, after that, any
  sequence of commands may be issued, in any order, including another PLACE
  command. The application should discard all commands in the sequence until
  a valid PLACE command has been executed.
- MOVE will move the toy robot one unit forward in the direction it is
  currently facing.
- LEFT and RIGHT will rotate the robot 90 degrees in the specified direction
  without changing the position of the robot.
- REPORT will announce the X,Y and F of the robot. This can be in any form,
  but standard output is sufficient.

- A robot that is not on the table can choose the ignore the MOVE, LEFT, RIGHT
  and REPORT commands.
- Input can be from a file, or from standard input, as the developer chooses.
- Provide test data to exercise the application.
- The application must be a command line application.  
  
##Constraints
  
  - The toy robot must not fall off the table during movement. This also
    includes the initial placement of the toy robot.
  - Any move that would cause the robot to fall must be ignored.
  
## Example Input and Output:
    
a)

	PLACE 0,0,NORTH
        LEFT
        REPORT


b)

	PLACE 1,2,EAST
        MOVE
        MOVE
        LEFT
        MOVE
        REPORT



##Compile, Test, Run and Packaging

- Compile: `mvn compile`

- Test: `mvn test`

- Packaging: `mvn package`, compiled jar in *target/* folder

- Run: `java -jar <compiled jar in target folder> <file path (optional)>`


###Run using a file input

	java -jar target/toy-robot-1.0-RELEASE.jar input.txt
	
###Run using commandline	
    java -jar target/toy-robot-1.0-RELEASE.jar



