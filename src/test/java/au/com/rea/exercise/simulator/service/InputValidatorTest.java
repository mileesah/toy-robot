package au.com.rea.exercise.simulator.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class InputValidatorTest {

    private InputValidator validator;

    @Before
    public void setup(){
        this.validator = new InputValidator();
    }

    @Test
    public void testValidPlaceInput(){
        String input = "PLACE 2,2,NORTH";
        Assert.assertTrue(validator.isInputValid(input));
    }

    @Test
    public void testValidPlaceInput2(){
        String input = "place 2,2,north";
        Assert.assertTrue(validator.isInputValid(input));
    }

    @Test
    public void testNegativePlaceInput(){
        String input = "PLACE -1,2,NORTH";
        Assert.assertFalse(validator.isInputValid(input));
    }

    @Test
    public void testNegativePlaceInput2(){
        String input = "PLACE 2,-2,NORTH";
        Assert.assertFalse(validator.isInputValid(input));

    }

    @Test
    public void testNegativePlaceInput3(){
        String input = "PLACE -2,-2,NORTH";
        Assert.assertFalse(validator.isInputValid(input));

    }

    @Test
    public void testInvalidPlaceInput(){
        String input = "PLACE -2,-2,-2,NORTH";
        Assert.assertFalse(validator.isInputValid(input));

    }

    @Test
    public void testInvalidPlaceInput2(){
        String input = "PLACE 2,2,NORTHee";
        Assert.assertFalse(validator.isInputValid(input));

    }

    @Test
    public void testNullPlaceInput2(){
        String input = "PLACE ,2,NORTHee";
        Assert.assertFalse(validator.isInputValid(input));

    }

    @Test
    public void testNullInput(){
        String input = null;
        Assert.assertFalse(validator.isInputValid(input));

    }

    @Test
    public void testEmptyInput(){
        String input = "";
        Assert.assertFalse(validator.isInputValid(input));

    }

    @Test
    public void testWhiteSpaceInput(){
        String input = "   ";
        Assert.assertFalse(validator.isInputValid(input));

    }

    @Test
    public void testNonExistentSpaceInput(){
        String input = "hide";
        Assert.assertFalse(validator.isInputValid(input));

    }


    /* Validate other commands */

    @Test
    public void testValidMoveInput(){
        String input = "Move";
        Assert.assertTrue(validator.isInputValid(input));
        input = "MOVE";
        Assert.assertTrue(validator.isInputValid(input));
        input = "move";
        Assert.assertTrue(validator.isInputValid(input));
    }


    @Test
    public void testInValidMoveInput(){
        String input = "Move 1";
        Assert.assertFalse(validator.isInputValid(input));
        input = "1 MOVE";
        Assert.assertFalse(validator.isInputValid(input));
        input = "move123";
        Assert.assertFalse(validator.isInputValid(input));
    }


    @Test
    public void testValidLeftInput(){
        String input = "left";
        Assert.assertTrue(validator.isInputValid(input));
        input = "LEFT";
        Assert.assertTrue(validator.isInputValid(input));
        input = "Left";
        Assert.assertTrue(validator.isInputValid(input));
    }


    @Test
    public void testInValidLeftInput(){
        String input = "Left 1";
        Assert.assertFalse(validator.isInputValid(input));
        input = "1 Left";
        Assert.assertFalse(validator.isInputValid(input));
        input = "Left123";
        Assert.assertFalse(validator.isInputValid(input));
    }


    @Test
    public void testValidRightInput(){
        String input = "right";
        Assert.assertTrue(validator.isInputValid(input));
        input = "RIGHT";
        Assert.assertTrue(validator.isInputValid(input));
        input = "right";
        Assert.assertTrue(validator.isInputValid(input));
    }


    @Test
    public void testInValidRightInput(){
        String input = "Right 1";
        Assert.assertFalse(validator.isInputValid(input));
        input = "1 Right";
        Assert.assertFalse(validator.isInputValid(input));
        input = "right123";
        Assert.assertFalse(validator.isInputValid(input));
    }

    @Test
    public void testValidReportInput(){
        String input = "report";
        Assert.assertTrue(validator.isInputValid(input));
        input = "REPORT";
        Assert.assertTrue(validator.isInputValid(input));
        input = "Report";
        Assert.assertTrue(validator.isInputValid(input));
    }


    @Test
    public void testInValidReportInput(){
        String input = "Report 1";
        Assert.assertFalse(validator.isInputValid(input));
        input = "1 Report";
        Assert.assertFalse(validator.isInputValid(input));
        input = "Report123";
        Assert.assertFalse(validator.isInputValid(input));
    }


}
