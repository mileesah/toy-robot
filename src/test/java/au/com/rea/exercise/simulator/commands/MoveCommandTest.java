package au.com.rea.exercise.simulator.commands;

import au.com.rea.exercise.simulator.model.Board;
import au.com.rea.exercise.simulator.model.Position;
import au.com.rea.exercise.simulator.service.Command;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MoveCommandTest {

    private Command moveCommand;

    @Before
    public void setup()
    {
        Board board = new Board(5,5);
        this.moveCommand = new MoveCommand(board);
    }


    @Test
    public void executeCommandWithPosition(){
        int x = 2;
        int y = 2;
        Position.Orientation orientation = Position.Orientation.EAST;
        Position position = new Position(x,y,orientation);

        position = moveCommand.execute(position);

        Assert.assertNotNull(position);
        Assert.assertEquals(3, position.getX());
        Assert.assertEquals(2, position.getY());
        Assert.assertEquals(orientation, position.getOrientation());

    }

    @Test
    public void executeCommandWithPosition2(){
        int x = 2;
        int y = 2;
        Position.Orientation orientation = Position.Orientation.WEST;
        Position position = new Position(x,y,orientation);

        position = moveCommand.execute(position);

        Assert.assertNotNull(position);
        Assert.assertEquals(1, position.getX());
        Assert.assertEquals(2, position.getY());
        Assert.assertEquals(orientation, position.getOrientation());

    }

    @Test
    public void executeCommandWithPosition3(){
        int x = 2;
        int y = 2;
        Position.Orientation orientation = Position.Orientation.SOUTH;
        Position position = new Position(x,y,orientation);

        position = moveCommand.execute(position);

        Assert.assertNotNull(position);
        Assert.assertEquals(2, position.getX());
        Assert.assertEquals(1, position.getY());
        Assert.assertEquals(orientation, position.getOrientation());

    }

    @Test
    public void executeCommandWithPosition4(){
        int x = 2;
        int y = 2;
        Position.Orientation orientation = Position.Orientation.NORTH;
        Position position = new Position(x,y,orientation);

        position = moveCommand.execute(position);

        Assert.assertNotNull(position);
        Assert.assertEquals(2, position.getX());
        Assert.assertEquals(3, position.getY());
        Assert.assertEquals(orientation, position.getOrientation());

    }


    @Test
    public void executeCommandWithInvalidPositionOnBoard(){
        int x = 0;
        int y = 0;
        Position.Orientation orientation = Position.Orientation.SOUTH;
        Position position = new Position(x,y,orientation);

        position = moveCommand.execute(position);

        Assert.assertNull(position);

    }
}
