package au.com.rea.exercise.simulator.commands;

import au.com.rea.exercise.simulator.model.Board;
import au.com.rea.exercise.simulator.model.Position;
import au.com.rea.exercise.simulator.service.Command;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class LeftCommandTest {
    private Command leftCommand;


    @Before
    public void setup(){
        Board board = new Board(5,5);
        this.leftCommand = new LeftCommand(board);
    }


    @Test
    public void executeCommandWithPosition(){
        int x = 2;
        int y = 2;
        Position.Orientation orientation = Position.Orientation.EAST;
        Position position = new Position(x,y,orientation);

        position = leftCommand.execute(position);

        Assert.assertNotNull(position);
        Assert.assertEquals(2, position.getX());
        Assert.assertEquals(2, position.getY());
        Assert.assertEquals(Position.Orientation.NORTH, position.getOrientation());

    }

    @Test
    public void executeCommandWithPosition2(){
        int x = 2;
        int y = 2;
        Position.Orientation orientation = Position.Orientation.NORTH;
        Position position = new Position(x,y,orientation);

        position = leftCommand.execute(position);

        Assert.assertNotNull(position);
        Assert.assertEquals(2, position.getX());
        Assert.assertEquals(2, position.getY());
        Assert.assertEquals(Position.Orientation.WEST, position.getOrientation());

    }

    @Test
    public void executeCommandWithPosition3(){
        int x = 2;
        int y = 2;
        Position.Orientation orientation = Position.Orientation.WEST;
        Position position = new Position(x,y,orientation);

        position = leftCommand.execute(position);

        Assert.assertNotNull(position);
        Assert.assertEquals(2, position.getX());
        Assert.assertEquals(2, position.getY());
        Assert.assertEquals(Position.Orientation.SOUTH, position.getOrientation());

    }

    @Test
    public void executeCommandWithPosition4(){
        int x = 2;
        int y = 2;
        Position.Orientation orientation = Position.Orientation.SOUTH;
        Position position = new Position(x,y,orientation);

        position = leftCommand.execute(position);

        Assert.assertNotNull(position);
        Assert.assertEquals(2, position.getX());
        Assert.assertEquals(2, position.getY());
        Assert.assertEquals(Position.Orientation.EAST, position.getOrientation());

    }


    @Test
    public void executeCommandWithInvalidPosition(){
        int x = 2;
        int y = 2;
        Position.Orientation orientation = null;
        Position position = null;

        position = leftCommand.execute(position);

        Assert.assertNull(position);


    }


    @Test
    public void executeCommandWithInvalidPositionOnBoard(){
        int x = 6;
        int y = 6;
        Position.Orientation orientation = Position.Orientation.SOUTH;
        Position position = new Position(x,y,orientation);

        position = leftCommand.execute(position);

        Assert.assertNull(position);

    }
}
