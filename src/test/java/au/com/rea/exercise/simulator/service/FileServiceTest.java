package au.com.rea.exercise.simulator.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.net.URL;
import java.util.List;

public class FileServiceTest {

    FileService fileService;

    @Before
    public void setup(){
        fileService  = new FileService();
    }


    @Test
    public void testValidFilePath(){
        URL url = this.getClass().getResource("/validInput.txt" );
        String filePath = url.getPath();
        Assert.assertTrue(fileService.isFileValid(filePath));
    }

    @Test
    public void testInvalidFilePath(){
        String filePath = "";
        Assert.assertFalse(fileService.isFileValid(filePath));
    }

    @Test
    public void testInvalidFilePath2(){
        String filePath = "  ";
        Assert.assertFalse(fileService.isFileValid(filePath));
    }

    @Test
    public void testInvalidFilePath3(){
        String filePath = null;
        Assert.assertFalse(fileService.isFileValid(filePath));
    }

    @Test
    public void testInvalidFilePath4(){
        String filePath = "C:/file";
        Assert.assertFalse(fileService.isFileValid(filePath));
    }


    @Test
    public void testValidInputFile(){
        URL url = this.getClass().getResource("/validInput.txt" );
        File file = new File(url.getPath());
        List<String> commandList = fileService.parseFile(file);
        Assert.assertNotNull(commandList);
        Assert.assertEquals(5, commandList.size());
        Assert.assertEquals("PLACE 1,2,EAST", commandList.get(0));
        Assert.assertEquals("MOVE", commandList.get(4));
    }

}
