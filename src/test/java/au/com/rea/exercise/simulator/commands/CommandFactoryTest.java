package au.com.rea.exercise.simulator.commands;


import au.com.rea.exercise.simulator.model.Board;
import au.com.rea.exercise.simulator.service.Command;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CommandFactoryTest {

    private final String LEFT  ="LEFT";
    private final String RIGHT  ="RIGHT";
    private final String MOVE  ="MOVE";
    private final String PLACE  ="PLACE";
    private final String REPORT  ="REPORT";
    private Board board;

    private CommandFactory commandFactory;

    @Before
    public void setup(){
        this.commandFactory = new CommandFactory();
        this.board = new Board();
    }


    @Test
    public void testGetLeftCommand(){
        Command command = commandFactory.getCommand(LEFT,board);
        Assert.assertNotNull(command);
        Assert.assertEquals(LeftCommand.class,command.getClass());
    }

    @Test
    public void testGetRightCommand(){
        Command command = commandFactory.getCommand(RIGHT,board);
        Assert.assertNotNull(command);
        Assert.assertEquals(RightCommand.class,command.getClass());
    }

    @Test
    public void testGetMoveCommand(){
        Command command = commandFactory.getCommand(MOVE,board);
        Assert.assertNotNull(command);
        Assert.assertEquals(MoveCommand.class,command.getClass());
    }

    @Test
    public void testGetReportCommand(){
        Command command = commandFactory.getCommand(REPORT,board);
        Assert.assertNotNull(command);
        Assert.assertEquals(ReportCommand.class,command.getClass());
    }

    @Test
    public void testGetPlaceCommand(){
        Command command = commandFactory.getCommand(PLACE,board);
        Assert.assertNotNull(command);
        Assert.assertEquals(PlaceCommand.class,command.getClass());
    }

    @Test
    public void testGetInvalidCommand(){
        Command command = commandFactory.getCommand("sdfasfdasd",board);
        Assert.assertNull(command);
    }

}
