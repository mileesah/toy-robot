package au.com.rea.exercise.simulator.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BoardTest {

    private  Board board;
    @Before
    public void setup(){
        board = new Board(5,5);
    }

    @Test
    public void testValidPosition(){
        Position position = new Position( 1,3, Position.Orientation.NORTH);
        Assert.assertTrue(board.isPositionValid(position));

    }

    @Test
    public void testValidPosition2(){
        Position position = new Position( 4,4, Position.Orientation.NORTH);
        Assert.assertTrue(board.isPositionValid(position));

    }


    @Test
    public void testInValidPosition(){
        Position position = new Position( -1,4, Position.Orientation.NORTH);
        Assert.assertFalse(board.isPositionValid(position));

    }

    @Test
    public void testInValidPosition2(){
        Position position = new Position( 1,-4, Position.Orientation.NORTH);
        Assert.assertFalse(board.isPositionValid(position));

    }

    @Test
    public void testInValidPosition3(){
        Position position = new Position( 5,5, Position.Orientation.NORTH);
        Assert.assertFalse(board.isPositionValid(position));

    }

    @Test
    public void testInValidPosition4(){

        Assert.assertFalse(board.isPositionValid(null));

    }

}