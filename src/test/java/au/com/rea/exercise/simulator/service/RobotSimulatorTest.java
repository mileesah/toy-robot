package au.com.rea.exercise.simulator.service;

import au.com.rea.exercise.simulator.RobotSimulator;
import au.com.rea.exercise.simulator.model.Position;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.net.URL;

public class RobotSimulatorTest {

    private RobotSimulator robotSimulator ;

    @Before
    public void setup(){
        robotSimulator = new RobotSimulator(5,5);
    }





    /* Test execute command*/
    @Test
    public void testExecutePlaceCommand(){
        String command = "PLACE 1,2,NORTH";
        Position position = robotSimulator.executeCommand(command);

        Assert.assertNotNull(position);
        Assert.assertEquals(1, position.getX());
        Assert.assertEquals(2, position.getY());
        Assert.assertEquals(Position.Orientation.NORTH, position.getOrientation());
    }

    @Test
    public void testExecutePlaceCommandSequence(){
        String command = "PLACE 1,2,NORTH";
        Position position = robotSimulator.executeCommand(command);
        command = "PLACE 4,2,WEST";
        position = robotSimulator.executeCommand(command);

        Assert.assertNotNull(position);
        Assert.assertEquals(4, position.getX());
        Assert.assertEquals(2, position.getY());
        Assert.assertEquals(Position.Orientation.WEST, position.getOrientation());
    }





    @Test
    public void testExecuteNullCommand(){
        String command = null;
        Position position = robotSimulator.executeCommand(command);


        Assert.assertNull(position);

    }


    @Test
    public void testExecuteNullCommand2(){
        String command = "PLACE 1,2,NORTH";
        Position position = robotSimulator.executeCommand(command);
        command = null;
        position = robotSimulator.executeCommand(command);


        Assert.assertNotNull(position);
        Assert.assertEquals(1, position.getX());
        Assert.assertEquals(2, position.getY());
        Assert.assertEquals(Position.Orientation.NORTH, position.getOrientation());

    }



    @Test
    public void testExecuteMoveCommandWithoutPlacing(){
        robotSimulator = new RobotSimulator();

        Position position = robotSimulator.executeCommand("MOVE");

        Assert.assertNull(position);


    }

    @Test
    public void testExecuteLeftCommandWithoutPlacing(){
        robotSimulator = new RobotSimulator();

        Position position = robotSimulator.executeCommand("LEFT");

        Assert.assertNull(position);


    }

    @Test
    public void testExecuteRightCommandWithoutPlacing(){
        robotSimulator = new RobotSimulator();

        Position position = robotSimulator.executeCommand("RIGHT");

        Assert.assertNull(position);


    }

    @Test
    public void testExecuteReportCommandWithoutPlacing(){
        robotSimulator = new RobotSimulator();

        Position position = robotSimulator.executeCommand("REPORT");

        Assert.assertNull(position);


    }

    @Test
    public void testValidExecuteLeftCommand(){
        String command = "PLACE 0,0,NORTH";
        Position position = robotSimulator.executeCommand(command);
        command = "LEFT";
        position = robotSimulator.executeCommand(command);


        Assert.assertNotNull(position);
        Assert.assertEquals(0, position.getX());
        Assert.assertEquals(0, position.getY());
        Assert.assertEquals(Position.Orientation.WEST, position.getOrientation());

    }

    @Test
    public void testValidExecuteRightCommand(){
        String command = "PLACE 0,0,NORTH";
        Position position = robotSimulator.executeCommand(command);
        command = "RIGHT";
        position = robotSimulator.executeCommand(command);


        Assert.assertNotNull(position);
        Assert.assertEquals(0, position.getX());
        Assert.assertEquals(0, position.getY());
        Assert.assertEquals(Position.Orientation.EAST, position.getOrientation());

    }

    @Test
    public void testValidExecuteMoveCommand(){
        String command = "PLACE 0,0,NORTH";
        Position position = robotSimulator.executeCommand(command);
        command = "Move";
        position = robotSimulator.executeCommand(command);


        Assert.assertNotNull(position);
        Assert.assertEquals(0, position.getX());
        Assert.assertEquals(1, position.getY());
        Assert.assertEquals(Position.Orientation.NORTH, position.getOrientation());

    }

    @Test
    public void testInValidExecuteMoveCommand(){
        String command = "PLACE 0,0,WEST";
        Position position = robotSimulator.executeCommand(command);
        command = "Move";
        position = robotSimulator.executeCommand(command);


        Assert.assertNotNull(position);
        Assert.assertEquals(0, position.getX());
        Assert.assertEquals(0, position.getY());
        Assert.assertEquals(Position.Orientation.WEST, position.getOrientation());

    }


    @Test
    public void testValidFileParsing(){
        URL url = this.getClass().getResource("/validInput.txt" );
        Position position = robotSimulator.executeFile(url.getPath());
        Assert.assertNotNull(position);
        Assert.assertEquals(3, position.getX());
        Assert.assertEquals(3, position.getY());
        Assert.assertEquals(Position.Orientation.NORTH, position.getOrientation());
    }

    @Test
    public void testValidFileParsingWithIgnore(){
        URL url = this.getClass().getResource("/inputWithIgnore.txt" );
        Position position = robotSimulator.executeFile(url.getPath());
        Assert.assertNotNull(position);
        Assert.assertEquals(3, position.getX());
        Assert.assertEquals(3, position.getY());
        Assert.assertEquals(Position.Orientation.NORTH, position.getOrientation());
    }

    @Test
    public void testValidFileParsingWithInvalidCommand(){
        URL url = this.getClass().getResource("/invalidInput.txt" );
        Position position = robotSimulator.executeFile(url.getPath());
        Assert.assertNotNull(position);
        Assert.assertEquals(3, position.getX());
        Assert.assertEquals(3, position.getY());
        Assert.assertEquals(Position.Orientation.NORTH, position.getOrientation());
    }
}
