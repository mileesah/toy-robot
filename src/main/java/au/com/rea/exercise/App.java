package au.com.rea.exercise;


import au.com.rea.exercise.simulator.RobotSimulator;

import java.util.Scanner;

public class App
{
    private final int DEFAULT_BOUNDARY_X =5;
    private final int DEFAULT_BOUNDARY_Y =5;
    private RobotSimulator simulator;

    public App() {
        simulator = new RobotSimulator(DEFAULT_BOUNDARY_X, DEFAULT_BOUNDARY_Y);
    }

    public static void main(String[] args )
    {
        App app = new App();
        app.simulate(args);
    }

    public void simulate(String[] args){
        if(args.length>0){
            System.out.println("Parsing Commands from "+args[0]);

            simulator.executeFile(args[0]);
        }else
        {
            this.run();

        }

    }


    private void run(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("To simulate robot movements, use the following commands: ");
        System.out.println("PLACE X,Y,(NORTH|SOUTH|EAST|WEST)\nMOVE\nLEFT\nRIGHT\nREPORT\n");
        System.out.println("TYPE EXIT to close program");

        String command ="";
        do{
            command = scanner.nextLine();
            simulator.executeCommand(command);

        }while (!command.equalsIgnoreCase("EXIT"));
    }
}
