package au.com.rea.exercise.simulator.commands;

import au.com.rea.exercise.simulator.model.Board;
import au.com.rea.exercise.simulator.model.Position;
import au.com.rea.exercise.simulator.service.Command;

public class PlaceCommand implements Command {

    private Board board;

    public PlaceCommand(Board board) {
        this.board = board;
    }

    @Override
    public Position execute(Position position) {
        return position;
    }
}
