package au.com.rea.exercise.simulator.service;

import java.util.Arrays;
import java.util.List;

/**
 * Validator class for robot commands
 */
public class InputValidator {

    private static final String PLACE_COMMAND = "PLACE";
    private static final String PLACE_COMMAND_REGEX = "^PLACE [0-9]+,[0-9]+,(NORTH|SOUTH|EAST|WEST)";
    private static final String OTHER_COMMAND_REGEX = "^(MOVE|LEFT|RIGHT|REPORT)";

    private static final List<String> COMMANDS = Arrays.asList(PLACE_COMMAND,"MOVE","LEFT","RIGHT", "REPORT");


    public boolean isInputValid(String input){
        if(input!=null){
            String sanitizedCommand = sanitizeCommand(input);

            if(!input.isEmpty() && isCommandValid(sanitizedCommand)){
                return true;
            }
        }
        return false;
    }


    public String sanitizeCommand(String input){
        return input.trim().toUpperCase();
    }

    private boolean isCommandValid(String command){
        String[] strSplit = command.split(" ");

        if(COMMANDS.contains(strSplit[0])){
            if(strSplit[0].equals(PLACE_COMMAND)){
                return command.matches(PLACE_COMMAND_REGEX);
            }
            return command.matches(OTHER_COMMAND_REGEX);
        }
        return false;

    }




}
