package au.com.rea.exercise.simulator.commands;

import au.com.rea.exercise.simulator.model.Board;
import au.com.rea.exercise.simulator.model.Position;
import au.com.rea.exercise.simulator.service.Command;

public class MoveCommand implements Command {

    private Board board;

    public MoveCommand(Board board) {
        this.board = board;
    }

    @Override
    public Position execute(Position position) {
        if(position!=null) {
            Position result =  new Position(position.getX(), position.getY(),position.getOrientation());

            switch (position.getOrientation()){
                case NORTH:
                    result.setY(position.getY()+1);
                    break;
                case SOUTH:
                    result.setY(position.getY()-1);
                    break;
                case EAST:
                    result.setX(position.getX()+1);
                    break;
                case WEST:
                    result.setX(position.getX()-1);
                    break;
                default:
                    break;
            }

            if(board.isPositionValid(result)){
                return result;
            }
        }
        return null;
    }
}
