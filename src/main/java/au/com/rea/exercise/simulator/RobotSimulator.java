package au.com.rea.exercise.simulator;

import au.com.rea.exercise.simulator.commands.CommandFactory;
import au.com.rea.exercise.simulator.model.Board;
import au.com.rea.exercise.simulator.model.Position;
import au.com.rea.exercise.simulator.model.Robot;
import au.com.rea.exercise.simulator.service.Command;
import au.com.rea.exercise.simulator.service.FileService;
import au.com.rea.exercise.simulator.service.InputValidator;

import java.io.File;
import java.util.List;

/**
 * Class that will simulate a robot on a table top
 */
public class RobotSimulator {

    private Board board;
    private Robot robot;
    private InputValidator inputValidator;
    private CommandFactory commandFactory;


    public RobotSimulator() {
        this(5,5);

    }

    /**
     *
     * @param boundary_x defines the width of the tabletop
     * @param boundary_y defines the height of the tabletop
     */
    public RobotSimulator(int boundary_x, int boundary_y) {
        this.board = new Board(boundary_x,boundary_y);
        this.robot = new Robot();
        this.inputValidator = new InputValidator();
        this.commandFactory = new CommandFactory();
    }

    /**
     * Executes a single command and returns the current position of the robot
     * @param commandStr
     * @return Position of the robot
     */
    public Position executeCommand(String commandStr){

        if(inputValidator.isInputValid(commandStr)){
            commandStr = inputValidator.sanitizeCommand(commandStr);
            String[] strSplit = commandStr.split(" ");

            Position position = robot.getPosition();
            Command command = commandFactory.getCommand(strSplit[0], board);
            if(strSplit[0].equals("PLACE")){
                String strArgs[] = strSplit[1].split(",");
                int x = Integer.parseInt(strArgs[0]);
                int y = Integer.parseInt(strArgs[1]);
                String face =  strArgs[2];
                position = new Position(x,y, Position.Orientation.valueOf(face));
            }else if (!isRobotPlaced()){
                return position;
            }
            position = command.execute(position);
            if(position!=null){
                robot.setPosition(position);

            }


        }
        return robot.getPosition();
    }

    /**
     * Parses a file to retrieve the set of commands to simulate the robot
     * @param path of the file with the commands
     * @return current position of the robot
     */
    public Position executeFile(String path){
        FileService fileService = new FileService();
        if(fileService.isFileValid(path)){
            File file = new File(path);
            List<String> commands = fileService.parseFile(file);

            for (String command:commands) {
                executeCommand(command);

            }

            return robot.getPosition();

        }
        return  null;
    }




    private  boolean isRobotPlaced(){
        return robot.getPosition()!=null;
    }

}
