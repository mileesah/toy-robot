package au.com.rea.exercise.simulator.model;

public class Board {

    private int boundary_x;
    private int boundary_y;

    public Board() {
        this(5,5);
    }

    public Board(int boundary_x, int boundary_y) {
        this.boundary_x = boundary_x;
        this.boundary_y = boundary_y;
    }

    public int getBoundary_x() {
        return boundary_x;
    }

    public void setBoundary_x(int boundary_x) {
        this.boundary_x = boundary_x;
    }

    public int getBoundary_y() {
        return boundary_y;
    }

    public void setBoundary_y(int boundary_y) {
        this.boundary_y = boundary_y;
    }


    /**
     * Validates if the position is still within the boundaries of the tabletop
     * @param position
     * @return true if position is still on the tabletop
     */
    public boolean isPositionValid(Position position){
        if(position!=null){
            if(position.getY()>=0 && position.getY()<this.boundary_y &&
                    position.getX()>=0 && position.getX()<this.boundary_x){
                return true;
            }
        }
        return false;
    }
}
