package au.com.rea.exercise.simulator.commands;

import au.com.rea.exercise.simulator.model.Board;
import au.com.rea.exercise.simulator.model.Position;
import au.com.rea.exercise.simulator.service.Command;

public class ReportCommand implements Command{

    private Board board;

    public ReportCommand(Board board) {
        this.board = board;
    }

    @Override
    public Position execute(Position position) {
        System.out.println(position.getX()+","+ position.getY()+","+ position.getOrientation().name());
        return position;
    }
}
