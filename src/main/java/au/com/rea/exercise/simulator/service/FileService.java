package au.com.rea.exercise.simulator.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileService {

    public boolean isFileValid(String path){
        if(path!=null && !path.trim().isEmpty()) {
            File file = new File(path);
            if (file.exists() && file.isFile()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Parse a valid file to retrieve a list of commands
     * Commands are still dirty and still needs to be validated
     * @param file
     * @return
     */
    public List<String> parseFile(File file){
        List<String> commandList = new ArrayList<>();


        try (Stream<String> stream = Files.lines(Paths.get(file.getAbsolutePath()))) {

            commandList = stream.collect(Collectors.toList());

        }catch (IOException e) {
            e.printStackTrace();
        }


        return commandList;
    }
}
