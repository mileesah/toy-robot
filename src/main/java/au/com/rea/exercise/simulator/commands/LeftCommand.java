package au.com.rea.exercise.simulator.commands;

import au.com.rea.exercise.simulator.model.Board;
import au.com.rea.exercise.simulator.model.Position;
import au.com.rea.exercise.simulator.service.Command;

public class LeftCommand  implements Command{

    private Board board;

    public LeftCommand(Board board) {
        this.board = board;
    }

    @Override
    public Position execute(Position position) {
        if(position!=null && position.getOrientation()!=null) {

           Position newPosition =  new Position(position.getX(), position.getY(),position.getOrientation().left());
           if(board.isPositionValid(newPosition)){
               return newPosition;
           }
        }
        return null;
    }
}
