package au.com.rea.exercise.simulator.service;

import au.com.rea.exercise.simulator.model.Position;

/**
 * Interface to implement custom robot commands
 */
public interface Command {

    public Position execute(Position position);
}
