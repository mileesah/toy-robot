package au.com.rea.exercise.simulator.commands;

import au.com.rea.exercise.simulator.model.Board;
import au.com.rea.exercise.simulator.service.Command;

public class CommandFactory {

    /**
     * Handles the creation the proper command type
     * @param command type of command to create
     * @return
     */
    public Command getCommand(String command, Board board){

        switch (command){
            case "MOVE" :
                return new MoveCommand(board);
            case "LEFT" :
                return new LeftCommand(board);
            case "RIGHT" :
                return new RightCommand(board);
            case "REPORT" :
                return new ReportCommand(board);
            case "PLACE" :
                return new PlaceCommand(board);
            default:
                return null;

        }

    }
}
