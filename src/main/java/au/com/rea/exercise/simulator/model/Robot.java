package au.com.rea.exercise.simulator.model;

public class Robot {


    private Position position;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
}
