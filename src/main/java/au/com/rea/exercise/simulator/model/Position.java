package au.com.rea.exercise.simulator.model;

/**
 * Represents a position on the tabletop
 */
public class Position {


    private int x,y;
    /**
     * The direction the item is facing
     */
    private Orientation orientation;


    public enum Orientation {
        WEST("WEST"),
        SOUTH("SOUTH"),
        EAST("EAST"),
        NORTH("NORTH");

        private String value;

        Orientation(String value) {
            this.value = value;
        }

        public String value() {
            return value;
        }

        public Orientation left() {
            switch (this) {
                case WEST:
                    return SOUTH;
                case SOUTH:
                    return EAST;
                case EAST:
                    return NORTH;
                case NORTH:
                    return WEST;
                default:
                    return null;
            }
        }

        public Orientation right() {
            switch (this) {
                case WEST:
                    return NORTH;
                case SOUTH:
                    return WEST;
                case EAST:
                    return SOUTH;
                case NORTH:
                    return EAST;
                default:
                    return null;
            }
        }


    }


    public Position(int x, int y, Orientation orientation) {
        this.x = x;
        this.y = y;
        this.orientation = orientation;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }
}
